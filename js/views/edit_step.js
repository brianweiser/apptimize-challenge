BYOA.Views.EditStep = Backbone.View.extend({
  events: {
    "click #submit": "addNewStepClickHandler"
  },
  template: JST["step_form"],
  
  initialize: function (options) {
    this.story = options.story;
  },
  
  render: function () {
    var renderedContent = this.template({
      step: this.model,
      story: this.story,
      submitText: "Edit step",
      title: "Edit step for story: "
    });
    this.$el.html(renderedContent);
    
    return this;
  },
  
  addNewStepClickHandler: function (event) {
    event.preventDefault();
    
    var title = this.$("#title").val();
    var text = this.$("#text").val();
    var choiceOneText = this.$("#choice_one_text").val();
    var choiceOneCid = this.$("#choice_one_cid").val();
    var choiceTwoText = this.$("#choice_two_text").val();
    var choiceTwoCid = this.$("#choice_two_cid").val();
    
    this.model.set({
      title: title,
      text: text,
      choiceOneText: choiceOneText,
      choiceOneCid: choiceOneCid,
      choiceTwoText: choiceTwoText,
      choiceTwoCid: choiceTwoCid
    });
    
    BYOA.Routers.router.navigate("story/" + this.story.cid, {trigger: true});
  }
});