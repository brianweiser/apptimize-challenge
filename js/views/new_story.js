BYOA.Views.NewStory = Backbone.View.extend({
  events: {
    "click #createNewStory": "createNewStoryClickHandler"
  },
  tagName: "form",
  template: JST["new_story"],
  
  render: function () {
    var renderedContent = this.template();
    this.$el.html(renderedContent);
    
    return this;
  },
  
  createNewStoryClickHandler: function (event) {
    event.preventDefault();
    
    var title = this.$("#title").val();
    var story = new BYOA.Models.Story({
      title: title
    });
    
    BYOA.Collections.stories.add(story);
    
    BYOA.Routers.router.navigate("story/" + story.cid, {trigger: true});
  }
});