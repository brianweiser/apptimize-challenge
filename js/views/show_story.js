BYOA.Views.ShowStory = Backbone.View.extend({
  template: JST["show_story"],
  
  render: function (){
    var renderedContent = this.template({
      story: this.model
    });
    this.$el.html(renderedContent);
    
    return this;
  }
});