this["JST"] = this["JST"] || {};

this["JST"]["new_story"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<h1>Create a story</h1>\n\n<label>Title\n  <input type="text" id="title">\n</label>\n\n<button id="createNewStory">Create New Story</button>';

}
return __p
};

this["JST"]["show_story"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<h1>' +
((__t = ( story.escape("title") )) == null ? '' : __t) +
'</h1>\n\n<h2>Steps</h2>\n<table>\n  <thead>\n    <tr>\n      <th>Cid</th>\n      <th>Title</th>\n      <th>Choice One Text</th>\n      <th>Choice One Cid</th>\n      <th>Choice Two Text</th>\n      <th>Choice Two Cid</th>\n      </tr>\n    </thead>\n  <tbody>\n    ';
 story.steps().each(function (step) { ;
__p += '\n      <tr>\n        <td>' +
((__t = ( step.cid )) == null ? '' : __t) +
'</td>\n        <td>\n          <a href="#story/' +
((__t = ( story.cid )) == null ? '' : __t) +
'/edit_step/' +
((__t = ( step.cid )) == null ? '' : __t) +
'">\n            ' +
((__t = ( step.escape("title") )) == null ? '' : __t) +
'\n          </a>\n        </td>\n        <td>' +
((__t = ( step.escape("choiceOneText") )) == null ? '' : __t) +
'</td>\n        <td>' +
((__t = ( step.escape("choiceOneCid") )) == null ? '' : __t) +
'</td>\n        <td>' +
((__t = ( step.escape("choiceTwoText") )) == null ? '' : __t) +
'</td>\n        <td>' +
((__t = ( step.escape("choiceTwoCid") )) == null ? '' : __t) +
'</td>\n      </tr>\n    ';
 }) ;
__p += '\n  </tbody>\n</table>\n\n<a href="#story/' +
((__t = ( story.cid )) == null ? '' : __t) +
'/new_step">New Step</a>';

}
return __p
};

this["JST"]["step_form"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<h1>' +
((__t = ( title + story.escape("title") )) == null ? '' : __t) +
'</h1>\n\n<form>\n  <div class="input-group">\n    <label>Title\n      <input type="text" id="title" value=\'' +
((__t = ( step.escape("title") )) == null ? '' : __t) +
'\'>\n    </label>\n  </div>\n  \n  <div class="input-group">\n    <label>Text\n      <textarea id="text">' +
((__t = ( step.escape("text") )) == null ? '' : __t) +
'</textarea>\n    </label>\n  </div>\n  \n  <div class="input-group">\n    <label>Choice one text\n      <input type="text" id="choice_one_text" value=\'' +
((__t = ( step.escape("choiceOneText") )) == null ? '' : __t) +
'\'>\n    </label>\n  </div>\n  \n  <div class="input-group">\n    <label>Choice one "cid" (available on story screen)\n      <input type="text" id="choice_one_cid" value=\'' +
((__t = ( step.escape("choiceOneCid") )) == null ? '' : __t) +
'\'>\n    </label>\n  </div>\n  \n  <div class="input-group">\n    <label>Choice two Text\n      <input type="text" id="choice_two_text" value=\'' +
((__t = ( step.escape("choiceTwoText") )) == null ? '' : __t) +
'\'>\n    </label>\n  </div>\n  \n  <div class="input-group">\n    <label>Choice two "cid" (available on story screen)\n      <input type="text" id="choice_two_cid" value=\'' +
((__t = ( step.escape("choiceTwoCid") )) == null ? '' : __t) +
'\'>\n    </label>\n  </div>\n  \n  <button id="submit">' +
((__t = ( submitText )) == null ? '' : __t) +
'</button>\n</form>';

}
return __p
};