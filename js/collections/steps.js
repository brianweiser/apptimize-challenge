BYOA.Collections.Steps = Backbone.Collection.extend({
  model: BYOA.Models.Step,
  
  initialize: function (options) {
    this.story = options.story;
  }
});