var BYOA = {
  Models: {},
  Collections: {},
  Views: {},
  Routers: {},
  Utl: {},
  
  initialize: function () {
    BYOA.Collections.stories = new BYOA.Collections.Stories();
    
    var $content = $("#content");
    BYOA.Routers.router = new BYOA.Routers.Router({
      $rootEl: $content
    });
    
    Backbone.history.start();
  }
};