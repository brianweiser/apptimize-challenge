BYOA.Models.Story = Backbone.Model.extend({
  steps: function () {
    if (this._steps === undefined) {
      this._steps = new BYOA.Collections.Steps([], {
        story: this
      });
    }
    
    return this._steps;
  }
});
