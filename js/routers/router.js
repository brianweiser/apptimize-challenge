BYOA.Routers.Router = Backbone.Router.extend({
  routes: {
    "": "newStory",
    "new": "newStory",
    "story/:story_id": "showStory",
    "story/:story_id/new_step": "newStep",
    "story/:story_id/edit_step/:step_id": "editStep"
  },
  
  initialize: function (options) {
    this.$rootEl = options.$rootEl;
  },
  
  editStep: function (storyId, stepId) {
    var story = BYOA.Collections.stories.get(storyId);
    var step = story.steps().get(stepId);
    
    var editStepView = new BYOA.Views.EditStep({
      model: step,
      story: story
    });
    
    this._swapViews(editStepView);
  },
  
  newStep: function (storyId) {
    var story = BYOA.Collections.stories.get(storyId);
    
    var newStepView = new BYOA.Views.NewStep({
      collection: story.steps(),
      story: story
    });
    
    this._swapViews(newStepView);
  },
  
  newStory: function () {
    var newStoryView = new BYOA.Views.NewStory();
    this._swapViews(newStoryView);
  },
  
  showStory: function (storyId) {
    var story = BYOA.Collections.stories.get(storyId);
    
    var showStoryView = new BYOA.Views.ShowStory({
      model: story
    });
    
    this._swapViews(showStoryView);
  },
  
  _swapViews: function (view) {
    this._currentView && this._currentView.remove();
    this._currentView = view;
    this.$rootEl.html(view.render().$el);
  }
});